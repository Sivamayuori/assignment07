//This is a program to add and multiply two given matrices
#include<stdio.h>

//Defining the values
int first[100][100], second[100][100], result[100][100];
int rows1, rows2, columns1, columns2;
int x, y, z, option;
int total = 0;

//Function to add and multiply two matrices
int addMatrices();
int multiplyMatrices();

int addMatrices(){
	//First matrix
	//Display the dimensions of the first matrix
	printf("\nPlease enter the dimensions (rows columns) of the first matrix: ");
	//Reading the entered dimensions
	scanf("%d %d", &rows1, &columns1);
	//Printing the elements of the first matrix
	puts("\nPlease enter the elements of the first matrix: \n");

	//Inserting the values to the first matrix
	for(x=0; x < rows1; x++){
		for(y=0; y < columns1; y++){
			//Reading the entered values
			scanf("%d", &first[x][y]);
		}
	}

	//Second matrix
	//Display the dimensions of the second matrix
	printf("\nPlease enter the dimensions (rows columns) of the second matrix: ");
	//Reading the entered dimensions
	scanf("%d %d", &rows2, &columns2);

	//Checking if the dimensions of the first matrix is equal to the second matrix
	if((columns1 != columns2) || (rows1 != rows2)){
	       //Display if it is not equal
	       printf("\nDimensions of both matrices should be EQUAL! \n");
       	       goto end;
	}

	//Printing the elements of the second matrix
	puts("\nPlease enter the elements of the second matrix: \n");
	
	//Inserting the values to the second matrix
	for(x=0; x < rows2; x++){
		for(y=0; y < columns2; y++){
			//Reading the entered values
			scanf("%d", &second[x][y]);
		}
	}

	//Result matrix
	for(x=0; x < rows1; x++){
		for(y=0; y < columns1; y++){
			result[x][y] = first[x][y] + second[x][y];
		}
	}
	//Display the result of the matrices
	printf("\nThe result is: \n\n");
	for(x=0; x < rows1; x++){
		for(y=0; y < columns1; y++){
			printf("%d ", result[x][y]);
		}
		printf("\n");
	}
	end:
	return 0;
}

int multiplyMatrices(){
	//First matrix
	//Display the dimensions of the first matrix
	printf("\nPlease enter the dimensions (rows columns) of the first matrix: ");
	//Reading the entered dimensions
	scanf("%d %d", &rows1, &columns1);
	//Printing the elements of the first matrix
	puts("\nPlease enter the elements of the first matrix: \n");

	//Inserting the elements to the first matrix
	for(x=0; x < rows1; x++){
		for(y=0; y < columns1; y++){
			scanf("%d", &first[x][y]);
		}
	}

	//Second matrix
	//Display the dimensions of the second matrix
	printf("\nPlease enter the dimensions (rows columns) of the second matrix: ");
	//Reading the entered dimensions
	scanf("%d %d", &rows2, &columns2);

	//Checking if the column count of the first matrix is equal to the row count of the second matrix
	if(columns1 != rows2){
		//Display if it is not equal
		printf("\nThe column count of the first matrix and the row count of the second matrix should be EQUAL! \n");
		goto end;
	}

	//Printing the elements of the second matrix
	puts("\nPlease enter the elements of the second matrix: \n");

	//Inserting the elements to the second matrix
	for(x=0; x < rows2; x++){
		for(y=0; y < columns2; y++){
			scanf("%d", &second[x][y]);
		}
	}

	//Result matrix
	for(x=0; x < rows1; x++){
		for(y=0; y <columns2; y++){
			for(z=0; z < rows2; z++){
				total += first[x][z] * second[z][y];
			}
			result[x][y] = total;
			total = 0;
		}
	}

	//Display the result
	printf("\nThe result is: \n\n");
	for(x=0; x < rows1; x++){
		for(y=0; y < columns2; y++){
			printf("%d", result[x][y]);
		}
		printf("\n");
	}
	end:
	return 0;
}

int main(){
	//Display the function to be performed
	printf("Please select the function to be performed: \n");
	//Display the options
	printf("(1) Add Matrices \n(2) Multiply Matrices \n\n");
	//Reading the chosen option
	scanf("%d", &option);

	if(option == 1){
		addMatrices();
	}else if(option == 2){
		multiplyMatrices();
	}else{
		printf("Invalid input! \n");
	}
	return 0;
}

