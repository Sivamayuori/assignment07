//This is a program to reverse a sentence
#include<stdio.h>

//Function to reverse the sentence
void reverseSentence();

int main (){
	//Display the sentence entered by the user
	printf("Please enter a sentence: ");
	reverseSentence();
	printf("\n");
	return 0;
}

void reverseSentence(){
	char character;
	
	//Reading the characters
	scanf("%c", &character);

	if(character != '\n'){
		reverseSentence();
		
		//Display the character
		printf("%c", character);
	}
}


