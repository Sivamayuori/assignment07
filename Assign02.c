//This is a program to find the frequency of a given character in a given string
#include<stdio.h>
int main(){
	char str[1000], ch;
	int i;
	int count=0;

	//Display the sentence
	printf("Please enter a string: ");
	fgets(str, sizeof(str), stdin);

	//Display the character's frequency to be found
	printf("Please enter a character to find its frequency: ");
	//Reading the entered character
	scanf("%c", &ch);

	for(int i=0; str[i] !='\0'; ++i){
		if(ch == str[i]){
			++count;
		}
		else{
			continue;
		}
	}

	//Display the frequency of the given character
	printf("Frequency of %c = %d", ch, count);

	printf("\n");
	return 0;
}

